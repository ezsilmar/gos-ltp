﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Linq_sample
    {
        public static void Foreach()
        {
            var a = new List<int>();
            foreach (var item in a)
            {
                Console.WriteLine(item);
            }
        }

        public static void Foreach_opened()
        {
            var a = new List<int>();
            using (var e = a.GetEnumerator())
            {
                while (e.MoveNext())
                {
                    Console.WriteLine(e.Current);
                }
            }
        }
    }

    class Node
    {
        public Node Next;
        public int Value;
    }
    class MyLinkedList : IEnumerable<int>
    {
        public Node root;

        public IEnumerator<int> GetEnumerator1()
        {
            var enumerator = new MyListEnumerator(){Cur = root};
            return enumerator;
        }

        class MyListEnumerator : IEnumerator<int>
        {
            public Node Cur;

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                if (Cur == null)
                {
                    return false;
                }
                Current = Cur.Value;
                Cur = Cur.Next;
                return true;
            }

            public void Reset()
            {
            }

            public int Current { get; private set; }

            object IEnumerator.Current
            {
                get { return Current; }
            }
        }

        public IEnumerator<int> GetEnumerator()
        {
            var cur = root;
            while (cur != null)
            {
                yield return cur.Value;
                cur = cur.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
