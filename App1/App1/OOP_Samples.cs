﻿// ReSharper disable InlineOutVariableDeclaration
// ReSharper disable RedundantAssignment
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable ArrangeAccessorOwnerBody
#pragma warning disable 649
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

using System;

namespace App1
{
    class SimpleCounter
    {
        private int value;

        public SimpleCounter(int initialValue)
        {
            value = initialValue;
        }

        public void Add(int amount)
        {
            value += amount;
        }

        public int Value
        {
            get { return value; }
        }

        public static SimpleCounter Zero()
        {
            return new SimpleCounter(0);
        }
    }

    class OOP_Samples
    { 
        public static void Main1()
        {
            var counter = new SimpleCounter(10);
            counter.Add(5);
            Console.WriteLine(counter.Value);

            var zero = SimpleCounter.Zero();

            zero.Increment();
        }
    }

    static class SimpleCounter_Extensions
    {
        public static void Increment(this SimpleCounter counter)
        {
            counter.Add(1);
        }
    }

    class Program_AnonymousClass
    {
        static void Main1()
        {
            var x = new {Value = 10, Name = "abc"};
            Console.WriteLine(x.Name + x.Value);
        }
    }

    class Program_Delegates
    {
        delegate void Add(int x);

        public static void Counter_Delegates()
        {
            var counter = new SimpleCounter(5);
            Add func = counter.Add;
            func(2);
            Do(func);
            Console.WriteLine(counter.Value);
        }

        static void Do(Add a)
        {
            a(5);
        }

        public static void Actions()
        {
            var counter = new SimpleCounter(0);
            Action<int> a = counter.Add;

            a(5);
        }

        public static void Funcs()
        {
            var counter = new SimpleCounter(0);
            Func<int, int> addAndReturn = delegate(int x)
            {
                counter.Add(x);
                return counter.Value;
            };

            int curValue = addAndReturn(3);
            Console.WriteLine(curValue);
        }

        public static void Lambda()
        {
            var counter = new SimpleCounter(0);
            Func<int, int> addAndReturn = (x) =>
            {
                counter.Add(x);
                return counter.Value;
            };
            Console.WriteLine(addAndReturn(3));

            Func<int> getValuePlusOne = () => counter.Value + 1;
            Console.WriteLine(getValuePlusOne());
        }
    }
}