﻿// ReSharper disable InlineOutVariableDeclaration
// ReSharper disable RedundantAssignment
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable ArrangeTypeMemberModifiers

using System;
using System.Collections.Generic;
using System.Text;

#pragma warning disable 649
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming


namespace App1
{
    class Program_Array
    {
        public static void Simple()
        {
            var x = new int[10];
            for (var i = 0; i < x.Length; i++)
            {
                x[i] = i;
            }
        }

        public static void IsRefType()
        {
            var x = new int[10];
            Set42(x, 5);
            Console.WriteLine(x[5]);
        }

        private static void Set42(int[] array, int idx)
        {
            array[idx] = 42;
        }

        public static void Multidimensional()
        {
            var x = new int[5, 10];
            var k = 0;
            for (var i = 0; i < x.GetLength(0); i++)
            {
                for (var j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] = k;
                    k++;
                }
            }
        }

        public static void ArrayOfArrays()
        {
            var x = new int[5][];
            for (var i = 0; i < x.Length; i++)
            {
                x[i] = new int[10];
            }

            var k = 0;
            for (var i = 0; i < x.Length; i++)
            {
                for (var j = 0; j < 10; j++)
                {
                    x[i][j] = k;
                    k++;
                }
            }
        }
    }

    class Program_List
    {
        public static void Main1()
        {
            var l = new List<int>();
            for (var i = 0; i < 10; i++)
            {
                l.Add(i);
            }

            foreach (var item in l)
            {
                Console.WriteLine(item);
            }
        }
    }

    class Program_String
    {
        public static void IsImmutable_Wrong()
        {
            var s = "abc";
            s.Replace('b', 'x');
            Console.WriteLine(s);
        }

        public static void IsImmutable_Correct()
        {
            var s = "abc";
            var result = s.Replace('b', 'x');
            Console.WriteLine(result);
        }

        public static void Concatenation_Slow()
        {
            var s = "";
            for (var i = 'a'; i < 'z'; i++)
            {
                s += i;
            }
            Console.WriteLine(s);
        }

        public static void Concatenation_Fast()
        {
            var sb = new StringBuilder();
            for (var i = 'a'; i < 'z'; i++)
            {
                sb.Append(i);
            }
            var s = sb.ToString();
            Console.WriteLine(s);
        }
    }

    class Program_StackAndQueue
    {
        public static void Main1()
        {
            var stack = new Stack<int>();
            var queue = new Queue<int>();

            for (var i = 0; i < 3; i++)
            {
                stack.Push(i);
                queue.Enqueue(i);
            }

            Console.WriteLine("Stack:");
            for (var i = 0; i < 3; i++)
            {
                Console.WriteLine(stack.Pop());
            }

            Console.WriteLine("Queue");
            for (var i = 0; i < 3; i++)
            {
                Console.WriteLine(queue.Dequeue());
            }
        }
    }

    class Program_Hash
    {
        void HashSet_Sample()
        {
            var set = new HashSet<int>();
            set.Add(1);
            set.Add(2);

            Console.WriteLine(set.Contains(1));
        }

        class A
        {
            public int X;
        }

        void HashSet_CustomClass()
        {
            var set = new HashSet<A>();
            set.Add(new A {X = 10});

            Console.WriteLine(set.Contains(new A{X = 10}));
        }

        class B
        {
            public int X;
            public int Y;

            #region Equality
            public override bool Equals(object obj)
            {
                var b = obj as B;
                return b != null &&
                       X == b.X &&
                       Y == b.Y;
            }

            public override int GetHashCode()
            {
                var hashCode = 1861411795;
                hashCode = hashCode * -1521134295 + X.GetHashCode();
                hashCode = hashCode * -1521134295 + Y.GetHashCode();
                return hashCode;
            }
            #endregion
        }

        void HashSet_CustomEquals()
        {
            var set = new HashSet<B>();
            var a = new B {X = 10, Y = 5};
            set.Add(a);

            var b = new B {X = 10, Y = 5};
            Console.WriteLine(set.Contains(b));
            Console.WriteLine(a.Equals(b));
            Console.WriteLine(a == b);
        }

        struct Point
        {
            public double X, Y;

            public override bool Equals(object obj)
            {
                if (!(obj is Point))
                {
                    return false;
                }
                var other = (Point) obj;
                var eps = 1e-5;
                return Math.Sqrt((X - other.X)*(X - other.X) + (Y - other.Y)*(Y - other.Y)) < eps;
            }

            public override int GetHashCode()
            {
                return 0;
            }
        }

        void HashSetForPoints()
        {
            var p1 = new Point {X = 0, Y = 0};
            var p2 = new Point {X = 1e-5, Y = 0};
            var p3 = new Point {X = 0, Y = 1e-5};

            Console.WriteLine(p1.Equals(p2));
            Console.WriteLine(p1.Equals(p3));
            Console.WriteLine(p2.Equals(p3));
        }

        void Dict_Sample()
        {
            var dict = new Dictionary<int, string>();
            dict.Add(1, "one");

            Console.WriteLine(dict[1]);
        }
    }
}