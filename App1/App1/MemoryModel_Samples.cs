﻿using System;

// ReSharper disable InlineOutVariableDeclaration
// ReSharper disable RedundantAssignment
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable ArrangeTypeMemberModifiers
#pragma warning disable 649
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

namespace App1
{
    class Program_StackFrames
    {
        public static void Main1()
        {
            A();
            B();
        }

        public static void A()
        {
            Console.WriteLine("A");
        }

        public static void B()
        {
            Console.WriteLine("B");
        }
    }

    class Program_LocalVariables
    {
        public static void Main1()
        {
            var a = 5;
            var b = 2;
            var c = a + b;
            Console.WriteLine(c);
        }
    }

    class Program_ParametersAndReturn
    {
        public static void Main1()
        {
            var a = 5;
            var b = 3;
            var c = Sum(a, b);
            Console.WriteLine($"{a} + {b} = {c}");
        }

        public static int Sum(int a, int b)
        {
            // a = 6;
            return a + b;
        }
    }

    class Program_RefParameter
    {
        public static void Main1()
        {
            var a = 5;
            var b = 3;
            var c = Sum(ref a, b);
            Console.WriteLine($"{a} + {b} = {c}");
        }

        public static int Sum(ref int a, int b)
        {
            a = 6;
            return a + b;
        }
    }

    class Program_OutParameter
    {
        public static void Main1()
        {
            var a = 5;
            var b = 3;
            int c;
            Sum(a, b, out c);
            Console.WriteLine($"{a} + {b} = {c}");
        }

        public static void Sum(int a, int b, out int c)
        {
            c = a + b;
        }
    }

    class Program_ReferenceType
    {
        public class A
        {
            public int X;
        }

        public static void Main1()
        {
            var a = new A{X = 5};
            PrintX(a);
            Console.WriteLine(a.X);
        }

        public static void PrintX(A a)
        {
            a.X = 10;
            Console.WriteLine(a.X);
        }
    }

    class Program_ValueType
    {
        public struct A
        {
            public int X;
        }

        public static void Main1()
        {
            var a = new A { X = 5 };
            PrintX(a);
            Console.WriteLine(a.X);
        }

        public static void PrintX(A a)
        {
            a.X = 10;
            Console.WriteLine(a.X);
        }
    }

    class Program_Null
    {
        class A
        {
            public B X;
        }

        class B
        {
            
        }

        public static void Main1()
        {
            var a = new A();
            Console.WriteLine(a.X == null);
        }
    }

    class Program_GC
    {
        class A
        {
            public int X;
        }

        public static void Main1()
        {
            for (var i = 0; i < 5; i++)
            {
                PrintA(i);
            }
        }

        private static void PrintA(int i)
        {
            var a2 = new A {X = i};
            Console.WriteLine(a2.X);
        }
    }
}
